defmodule MultihostRoutesWeb.FooLiveTest do
  use MultihostRoutesWeb.ConnCase

  import Phoenix.LiveViewTest

  setup :on_app_host

  describe ":index" do
    test "static and connected render", %{conn: conn} do
      {:ok, index_live, html} = live(conn, Routes.foo_path(conn, :index) |> with_app_host())
      assert html =~ "Foo!"
      assert element(index_live, "h1", "Foo!") |> has_element?()
    end
  end

  describe ":show" do
    test "static and connected render", %{conn: conn} do
      {:ok, show_live, html} = live(conn, Routes.foo_path(conn, :show) |> with_app_host())
      assert html =~ "Foo!"
      assert element(show_live, "h1", "Foo!") |> has_element?()
    end
  end

  describe "Patching between views on the same subdomain" do
    test "A: using full URLs", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.foo_path(conn, :index))

      index_live
      |> element("#A2")
      |> render_click()

      assert_patch(
        index_live,
        Routes.foo_path(conn, :show) |> with_app_host()
      )
    end

    test "B: using a naked paths", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.foo_path(conn, :index))

      index_live
      |> element("#B2")
      |> render_click()

      assert_patch(
        index_live,
        Routes.foo_path(conn, :show)
      )
    end
  end
end
