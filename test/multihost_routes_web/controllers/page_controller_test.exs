defmodule MultihostRoutesWeb.PageControllerTest do
  use MultihostRoutesWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 302)

    conn = get(conn, "/" |> with_app_host())
    assert html_response(conn, 200) =~ "Hi there!"
  end
end
