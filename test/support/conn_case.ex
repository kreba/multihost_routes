defmodule MultihostRoutesWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use MultihostRoutesWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import MultihostRoutesWeb.ConnCase
      import MultihostRoutesWeb.Hosts

      alias MultihostRoutesWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint MultihostRoutesWeb.Endpoint
    end
  end

  setup(_tags) do
    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end

  @doc """
  Setup helper that sets the `conn`'s host to the "app" subdomain.
  """
  def on_app_host(%{conn: conn}), do: %{conn: %{conn | host: MultihostRoutesWeb.Hosts.app_host()}}

  @doc """
  Setup helper that sets the `conn`'s host to the "www" subdomain.
  """
  def on_www_host(%{conn: conn}), do: %{conn: %{conn | host: MultihostRoutesWeb.Hosts.www_host()}}
end
