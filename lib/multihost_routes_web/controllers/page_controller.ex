defmodule MultihostRoutesWeb.PageController do
  use MultihostRoutesWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
