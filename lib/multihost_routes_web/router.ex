defmodule MultihostRoutesWeb.Router do
  use MultihostRoutesWeb, :router

  import MultihostRoutesWeb.Hosts

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {MultihostRoutesWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", MultihostRoutesWeb, host: www_host() do
    pipe_through :browser

    get "/", PageController, :index, as: :www_index_page
    get "/about", PageController, :about
  end

  scope "/", MultihostRoutesWeb, host: app_host() do
    pipe_through :browser

    get "/", PageController, :index, as: :app_index_page

    live_session :app do
      live "/foo", FooLive, :index
      live "/foo/1", FooLive, :show
    end
  end
end
