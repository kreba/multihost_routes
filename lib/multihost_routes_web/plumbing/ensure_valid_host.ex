defmodule MultihostRoutesWeb.Plumbing.EnsureValidHost do
  @moduledoc """
  A Plug to make sure that all requests are made to an URL with one of the application's known hostnames.
  If a request does not match any of the given hostnames (strings only), it is redirected to the first host.

  We use it to redirect from the apex domain `example.com` to the subdomain `www.example.com`.

  ## Examples

      EnsureValidHost.call(conn, [host1, host2])
  """

  @behaviour Plug

  alias Plug.Conn

  @impl true
  def init(known_hosts) when is_list(known_hosts), do: known_hosts

  @impl true
  def call(%Conn{} = conn, known_hosts) do
    if conn.host in known_hosts do
      conn
    else
      conn
      |> redirect_to_first_host(known_hosts)
      |> Conn.halt()
    end
  end

  defp redirect_to_first_host(conn, [host | _]) do
    current_path = Phoenix.Controller.current_path(conn)
    target_url = MultihostRoutesWeb.Hosts.with_host(current_path, host)
    conn |> Phoenix.Controller.redirect(external: target_url)
  end
end
