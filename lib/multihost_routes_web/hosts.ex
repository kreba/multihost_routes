defmodule MultihostRoutesWeb.Hosts do
  @moduledoc "View helpers that ease dealing with our multiple subdomains"

  @doc "Typically 'example.com' or 'example.local'."
  def apex_domain(), do: Application.fetch_env!(:multihost_routes, :host_apex_domain)

  @doc "Typically 'int-' or an empty string."
  def env_prefix(), do: Application.fetch_env!(:multihost_routes, :host_env_prefix)

  @doc "CORS helper"
  def origin_allowed?(%URI{host: host}), do: host in known_hosts()

  @doc "Enumeration of all valid domain names (with prepended subdomains)"
  def known_hosts(), do: [www_host(), app_host()]

  @doc "The full domain name for all the pages that are behind a login"
  def app_host(), do: hostname_for("app")

  @doc "The full domain name for all static, public pages"
  def www_host(), do: hostname_for("www")

  defp hostname_for(subdomain), do: env_prefix() <> subdomain <> "." <> apex_domain()

  @doc "Helper function to convert a Routes.xyz_path() into a full URL using the 'app' subdomain"
  def with_app_host(path) when is_binary(path), do: path |> with_host(app_host())

  @doc "Helper function to convert a Routes.xyz_path() into a full URL using the 'www' subdomain"
  def with_www_host(path) when is_binary(path), do: path |> with_host(www_host())

  def with_host(path, host) when is_binary(path) and is_binary(host) do
    # As described in: https://hexdocs.pm/phoenix/Phoenix.Controller.html#current_url/2-custom-url-generation
    base_uri = MultihostRoutesWeb.Endpoint.struct_url()
    MultihostRoutesWeb.Router.Helpers.url(%URI{base_uri | host: host}) <> path
  end
end
