# MultihostRoutes

This Elixir app illustrates our way of dealing with multiple subdomains in the same Phoenix app.

It also explains two issues that currently exist with multi-host setups in Phoenix:

1. The `_url` route helpers do not respect the `host` that was set in the `router.ex`
2. Linking between pages on the same subdomain using `live_patch` works fine with paths only, but tests require full URLs
           

## Setup

Since this app uses multiple subdomains, your development setup can't just use `localhost`.
Your browser must be able to resolve the domain names for both subdomains. 

To that end you can use the following line in your `hosts` file.
(This file is available on all operating systems: Linux, Mac OSX and Windows.)

```
# multihost_routes (demo app for a Phoenix router issue)
127.0.0.1 www.multihost_routes.local app.multihost_routes.local
```

(You don't need to type the full domain into the browser address bar. 
The app is configured to take you there from `localhost:4000`. 
So you can click the link in the server log as usual.)
               
This app uses no database. 

Just run `mix deps.get` and you are ready for `mix phx.server`.
                                                                   

## Thanks!

Thank you for taking the time to look at this. 

And if you can help fix these two issues, that will be highly appreciated!
